//
//  ActorPickerView.h
//  Ritualistic
//
//  Created by Chris Gerber on 5/2/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActorPickerDelegate;
@class Script;

@interface ActorPickerView : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate> {
}

@property (nonatomic, strong) Script *script;
@property (nonatomic, strong) NSArray *actors;
@property (nonatomic, strong) NSString *defaultActor;
@property (nonatomic, strong) IBOutlet UIPickerView *actorPicker;
@property (nonatomic, strong) id<ActorPickerDelegate> delegate;

- (void)save;
- (void)cancel;
- (void)requestUserAbort:(NSError *)error;

@end

@protocol ActorPickerDelegate <NSObject>

- (void)actorPickerView:(ActorPickerView *)actorPickerView didPickActor:(NSString *)actor;

@end