//
//  ActorPickerView.m
//  Ritualistic
//
//  Created by Chris Gerber on 5/2/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "ActorPickerView.h"
#import "Script.h"
#import "Line.h"

@implementation ActorPickerView

@synthesize script=_script;
@synthesize actors=_actors;
@synthesize defaultActor=_defaultActor;
@synthesize actorPicker=_actorPicker;
@synthesize delegate=_delegate;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set the view title
    self.navigationItem.title = @"Select Actor";
    
    // create the cancel button
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = cancelButtonItem;
    
    // create the select button
    UIBarButtonItem *selectButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStyleBordered target:self action:@selector(save)];
    self.navigationItem.rightBarButtonItem = selectButtonItem;
    
    // create a database fetch to get the actors for the script
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Line"
                                              inManagedObjectContext:[_script managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"scripts == %@",
                              _script];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    
    // fetch all actors (not unique) into an array
    NSError *error = nil;
    NSArray *lineArray = [[NSArray alloc] initWithArray:[[_script managedObjectContext] executeFetchRequest:fetchRequest error:&error]];
    
    if (lineArray == nil) {
        // error fetching the actors
        [self requestUserAbort:error];
    } else {
        // use a dictionary to remove duplicate actors
        NSMutableDictionary *actors = [[NSMutableDictionary alloc] init];
        for (Line *line in lineArray) {
            // set the key of each actor's name to a value of the actor's name
            [actors setValue:line.actor forKey:line.actor];
        }
        // save the unique list of actors in our local array, sorted alphabetically
        NSArray *uniqueActorArray = [[NSArray alloc] initWithArray:[actors keysSortedByValueUsingSelector:@selector(caseInsensitiveCompare:)]];
        self.actors = uniqueActorArray;
        
        // search the actors for our default value
        NSInteger defaultActorIndex = 0;
        BOOL defaultActorFound = NO;
        for (NSUInteger index = 0; index < [self.actors count]; index++) {
            if ([[self.actors objectAtIndex:index] isEqualToString:self.defaultActor]) {
                // we found the actor; save their index number in the picker
                defaultActorIndex = index;
                defaultActorFound = YES;
            }
        }
        if (defaultActorFound) {
            // if we found our default actor, spin the picker to that name
            [self.actorPicker selectRow:defaultActorIndex+1 inComponent:0 animated:NO];
        }
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.script = nil;
    self.actors = nil;
    self.defaultActor = nil;
    self.actorPicker = nil;
    self.delegate = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}


#pragma mark - Picker View Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    // we only have one "column" in the picker
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    // the number of actors, plus "<none>"
    return [self.actors count]+1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    // return the actor from the array, plus "<none>" in slot 0
    if (row == 0) {
        return @"<none>";
    }
    return [self.actors objectAtIndex:row-1];
}

#pragma mark - Navigation controller helpers

/**
 Save the new actor setting.
 */
- (void)save
{
    // get the row of the selected actor
    NSInteger row = [self.actorPicker selectedRowInComponent:0];
    
    
    if  (row == 0) {
        // picked "<none>"; return an empty string
        [self.delegate actorPickerView:self didPickActor:@""];
    } else if (row > 0) {
        // return the actual actor's name
        [self.delegate actorPickerView:self didPickActor:[self.actors objectAtIndex:row-1]];
    } else {
        // return nil to indicate no actor selected
        [self.delegate actorPickerView:self didPickActor:nil];
    }
}

/**
 Abandon the new actor setting.
 */
- (void)cancel
{
    // return nil to indicate no actor selected
	[self.delegate actorPickerView:self didPickActor:nil];
}

#pragma mark - Helper functions

/**
 If an unrecoverable error occurs, notify the user to quit the application
 */
- (void)requestUserAbort:(NSError *)error
{
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unresolved error" message:[NSString stringWithFormat:@"Please press the home button to exit the application. (%@)",[error description]] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alert show];
}

@end
