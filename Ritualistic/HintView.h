//
//  HintView.h
//  Ritualistic
//
//  Created by Chris Gerber on 5/2/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Line.h"

@interface HintView : UIViewController {
}

@property (nonatomic, strong) Line *line;
@property (nonatomic, strong) IBOutlet UITextView *originalTextView;

- (id)initWithLine:(Line *)line;

@end
