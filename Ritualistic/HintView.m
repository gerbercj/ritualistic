//
//  HintView.m
//  Ritualistic
//
//  Created by Chris Gerber on 5/2/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "HintView.h"
#import "Line.h"

@implementation HintView

@synthesize line = _line;
@synthesize originalTextView = _originalTextView;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (id)initWithLine:(Line *)line
{
    self = [super init];
    
    // save the provided line
    if (_line != line) {
        _line = line;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set the title to the actor
    self.navigationItem.title = self.line.actor;
    
    // create the close button
    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
    self.navigationItem.rightBarButtonItem = closeButtonItem;
    
    // set the textview to the non-cipher, un-masked text
    self.originalTextView.text = self.line.text;
}

- (void)viewDidUnload
{
    self.line = nil;
    self.originalTextView = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}


#pragma mark - Navigation controller actions

- (void)close
{
    // hide the view
    [self dismissModalViewControllerAnimated:YES]; 
}

@end