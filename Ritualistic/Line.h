//
//  Line.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright (c) 2011 theGerb.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Script;

@interface Line : NSManagedObject {
@private
}
@property (nonatomic, strong) NSNumber * index;
@property (nonatomic, strong) NSString * text;
@property (nonatomic, strong) NSString * cipher;
@property (nonatomic, strong) NSNumber * isHighlighted;
@property (nonatomic, strong) NSString * actor;
@property (nonatomic, strong) Script * scripts;

@end
