//
//  Line.m
//  Ritualistic
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright (c) 2011 theGerb.com. All rights reserved.
//

#import "Line.h"
#import "Script.h"


@implementation Line
@dynamic index;
@dynamic text;
@dynamic cipher;
@dynamic isHighlighted;
@dynamic actor;
@dynamic scripts;


@end
