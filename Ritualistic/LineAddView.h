//
//  LineAddView.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/23/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActorPickerView.h"

@protocol LineAddDelegate;
@class Line;


@interface LineAddView : UIViewController <UITextFieldDelegate, ActorPickerDelegate>
{
@private
    BOOL keyboardVisible;
    BOOL initialLoad;
}

@property (nonatomic, strong) Line *line;
@property (nonatomic, assign) BOOL isExistingLine;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UITextField *actorTextField;
@property (nonatomic, strong) IBOutlet UITextView *lineTextView;
@property (nonatomic, strong) IBOutlet UISwitch *useAutoCipher;
@property (nonatomic, strong) IBOutlet UILabel *cipherTextLabel;
@property (nonatomic, strong) IBOutlet UITextView *cipherTextView;
@property (nonatomic, strong) id<LineAddDelegate> delegate;

- (IBAction)toggleUseAutoCipher:(id)sender;
- (IBAction)selectActorClicked:(id)sender;

- (void)updateCipherEnabled:(BOOL)enabled;

- (void)save;
- (void)cancel;
- (void)requestUserAbort:(NSError *)error;

@end


@protocol LineAddDelegate <NSObject>

- (void)lineAddView:(LineAddView *) lineAddView didAddLine:(Line *)line;

@end