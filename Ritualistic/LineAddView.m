//
//  LineAddView.m
//  Ritualistic
//
//  Created by Chris Gerber on 4/23/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "LineAddView.h"
#import "Line.h"
#import "NSString+Ritualistic.h"
#import "UIView+FindFirstResponder.h"
#import "ActorPickerView.h"

#define LINE_ADD_SCROLL_VIEW_HEIGHT 330

@implementation LineAddView

@synthesize line=_line;
@synthesize isExistingLine=_isExistingLine;
@synthesize scrollView=_scrollView;
@synthesize actorTextField=_actorTextField;
@synthesize lineTextView=_lineTextView;
@synthesize useAutoCipher=_useAutoCipher;
@synthesize cipherTextLabel=_cipherTextLabel;
@synthesize cipherTextView=_cipherTextView;
@synthesize delegate=_delegate;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set the view title
    self.navigationItem.title = @"Add Line";
    
    // create the cancel button
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = cancelButtonItem;
    
    // create the save button
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(save)];
    self.navigationItem.rightBarButtonItem = saveButtonItem;

    // default to autocipher
    self.useAutoCipher.on = YES;
    self.cipherTextView.editable = NO;
    
    // only load default values on initial load, not each appear
    initialLoad = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    // keyboard not visible yet
    keyboardVisible = NO;

    // register notifiers for change in keyboard state
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    // leave the fields blank, unless we're editing an existing line
    if (self.isExistingLine) {
        if (initialLoad) {
            self.actorTextField.text = self.line.actor;
            self.lineTextView.text = self.line.text;
            // set value of useAutoCipher switch based upon existance of cipher
            if (self.line.cipher == nil ) {
                self.cipherTextView.text = nil;
                self.useAutoCipher.on = YES;
            } else {
                self.cipherTextView.text = self.line.cipher;
                self.useAutoCipher.on = NO;
            }
            initialLoad = NO;
        }
    }

    // make the cipher textview enabled, or disabled
    [self updateCipherEnabled:!_useAutoCipher.on];
}

- (void)viewWillDisappear:(BOOL)animated
{
    // release notifiers for changes in keyboard state
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
}

- (void)viewDidUnload
{
    self.line = nil;
    self.scrollView = nil;
    self.actorTextField = nil;
    self.lineTextView = nil;
    self.useAutoCipher = nil;
    self.cipherTextLabel = nil;
    self.cipherTextView = nil;
    self.delegate = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

/**
 Readjust the scrollview height to make sure that user can access all controls.
 */
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    // adjust the scroll view height on rotation, potentially enabling scroll bars
    CGRect bounds = self.scrollView.bounds;
    bounds.size.height = LINE_ADD_SCROLL_VIEW_HEIGHT;
    self.scrollView.contentSize = bounds.size;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}


#pragma mark - Keyboard helper functions

/**
 Use the return button to resign the first responder in TextViews.
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    // based upon https://discussions.apple.com/message/7884934?messageID=7884934
    
    // if user hits enter, close the textview; user has to create a new line of dialog for the same actor if they want to change paragraphs
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

/**
 Use the return button to resign the first responder in TextFields.
 */
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // hide the keyboard
    [textField resignFirstResponder];
    return YES;
}

/**
 Resize the scrollview to take into account keyboard size.
 */
- (void)keyboardDidShow:(NSNotification *)notification
{
    // based upon http://iphonedevelopertips.com/user-interface/adjust-textfield-hidden-by-keyboard.html
    
    if (keyboardVisible) return;
    
    // Get the size of the keyboard.
    NSValue* aValue = [[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    NSUInteger keyboardHeight;
    switch (self.interfaceOrientation) {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            keyboardHeight = keyboardSize.height;
            break;
        default:
            keyboardHeight = keyboardSize.width;
            break;
    }

    // Resize the scroll view to make room for the keyboard
    CGRect viewFrame = self.scrollView.frame;
    viewFrame.size.height -= keyboardHeight;
    self.scrollView.frame = viewFrame;
    
    // Move the correct control into view
    UIView *firstResponder = self.scrollView.findFirstResponder;
    [self.scrollView scrollRectToVisible:firstResponder.frame animated:YES];

    // Keyboard is now visible
    keyboardVisible = YES;
}

/**
 Resize the scrollview to take into account keyboard size.
 */
- (void)keyboardWillHide:(NSNotification *)notification
{
    // based upon http://iphonedevelopertips.com/user-interface/adjust-textfield-hidden-by-keyboard.html
    
    if (!keyboardVisible) return;
    
    // Get the size of the keyboard.
    NSValue* aValue = [[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    NSUInteger keyboardHeight;
    switch (self.interfaceOrientation) {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            keyboardHeight = keyboardSize.height;
            break;
        default:
            keyboardHeight = keyboardSize.width;
            break;
    }
    
    // Reset the height of the scroll view to its original value
    CGRect viewFrame = self.scrollView.frame;
    viewFrame.size.height += keyboardHeight;
    self.scrollView.frame = viewFrame;

    // Keyboard is no longer visible
    keyboardVisible = NO;	
}

#pragma mark - UI actions and helpers

/**
 Switch back-and-forth from Auto Cipher mode, disabling/enabling associated controls as needed.
 */
- (IBAction)toggleUseAutoCipher:(id)sender
{
    if (_useAutoCipher.on) {
        // using auto cipher; can't edit text and value is blank
        self.cipherTextView.text = nil;
        self.cipherTextView.editable = NO;
    } else {
        // using manual cipher; can edit text and default to generated cipher
        self.cipherTextView.text = [_lineTextView.text generateCipher];
        self.cipherTextView.editable = YES;
    }
    
    // disable or enable the fields
    [self updateCipherEnabled:!_useAutoCipher.on];
}

/**
 Disable/enable cipher controls based upon Auto Cipher mode.
 */
- (void)updateCipherEnabled:(BOOL)enabled;
{
    // disable cipher text field if auto-cipher is on
    self.cipherTextLabel.enabled = enabled;
    self.cipherTextView.editable = enabled;
}

/**
 Pop-up a custom picker view to update the actor
 */
- (IBAction)selectActorClicked:(id)sender
{
    // get the actor picker view
    ActorPickerView *actorPickerView = [[ActorPickerView alloc] initWithNibName:@"ActorPickerView" bundle:nil];
    actorPickerView.delegate = self;
    
    // set the default actor, and the script to load the actors from
    actorPickerView.defaultActor = self.actorTextField.text;
    actorPickerView.script = self.line.scripts;
    
    // display the picker
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:actorPickerView];
    [self presentModalViewController:navigationController animated:YES];
    
}

/**
 Delegate to respond to the custom picker selection.
 */
- (void)actorPickerView:(ActorPickerView *)actorPickerView didPickActor:(NSString *)actor
{
    // if the user didn't cancel
    if (actor != nil) {
        // an actor was selected
        if ([actor isEqualToString:@""]) {
            // the "empty" actor ("<none>") was selected, so clear value
            self.actorTextField.text = nil;
        } else {
            // an actual actor was selected, so use it
            self.actorTextField.text = actor;
        }
    }
    
    // hide the view controller
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - Navigation controller actions

/**
 Save the new line entry, or line entry edits.
 */
- (void)save
{
    // save the values from the fields in the Core Data object
    self.line.actor = self.actorTextField.text;
    self.line.text = self.lineTextView.text;
    
    // if auto generating cipher text, store that in the database too
    if (self.useAutoCipher.on) {
        self.line.cipher = [self.line.text generateCipher];
    } else {
        self.line.cipher = self.cipherTextView.text;
    }
    
    // save the record to Core Data
	NSError *error = nil;
	if (![self.line.managedObjectContext save:&error]) {
        [self requestUserAbort:error];
	}		
    
    // tell the view to close
	[self.delegate lineAddView:self didAddLine:self.line];
}

/**
 Abandon the newly created line entry, or line entry edits.
 */
- (void)cancel
{
	if (!self.isExistingLine) {
        // only delete record if it was a temporary new object
        [self.line.managedObjectContext deleteObject:self.line];
        
        // update Core Data
        NSError *error = nil;
        if (![self.line.managedObjectContext save:&error]) {
            [self requestUserAbort:error];
        }		
    }
    
    // tell the view to close
    [self.delegate lineAddView:self didAddLine:nil];
}

#pragma mark - Helper functions

/**
 If an unrecoverable error occurs, notify the user to quit the application
 */
- (void)requestUserAbort:(NSError *)error
{
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unresolved error" message:[NSString stringWithFormat:@"Please press the home button to exit the application. (%@)",[error description]] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alert show];
}

@end
