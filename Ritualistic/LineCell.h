//
//  LineCell.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Line.h"
#import "Script.h"

@interface LineCell : UITableViewCell {
    BOOL isHighlight;
    BOOL isActor;
}

@property (nonatomic, strong) IBOutlet UILabel *actorLabel;
@property (nonatomic, strong) IBOutlet UILabel *lineLabel;

- (id)initWithCellIdentifier:(NSString *)cellIdentifier andFontSize:(CGFloat)fontSize;
- (void)setLine:(Line *)line forScript:(Script *)script;
- (void)applyHighlights;

+ (NSString *)cellTextForLine:(Line *)line usingScript:(Script *)script;

@end
