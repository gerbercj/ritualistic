//
//  LineCell.m
//  Ritualistic
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "LineCell.h"
#import "Line.h"
#import "Script.h"
#import "NSString+Ritualistic.h"

@implementation LineCell

@synthesize actorLabel=_actorLabel;
@synthesize lineLabel=_lineLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    return self;
}

/**
 This is the standard init method that creates our custom cells.  It takes an identifier to use for caching, and a font size to make someday support a font size slider.
 */
- (id)initWithCellIdentifier:(NSString *)cellIdentifier andFontSize:(CGFloat)fontSize
{
    // create a new cell
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    // set automatic resizing to support landscape
    [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    // create the actor label
    CGRect actorFrame = CGRectMake(5, 4, 68, 24);
    _actorLabel = [[UILabel alloc] initWithFrame:actorFrame];
    [_actorLabel setLineBreakMode:UILineBreakModeClip];
    [_actorLabel setFont:[UIFont systemFontOfSize:fontSize]];
    [self.contentView addSubview:_actorLabel];
    
    // create the text label (for line, cipher, or masked text)
    CGRect textFrame = CGRectMake(75, 10, 235, 24);
    _lineLabel = [[UILabel alloc] initWithFrame:textFrame];
    [_lineLabel setFont:[UIFont systemFontOfSize:fontSize]];
    [_lineLabel setLineBreakMode:UILineBreakModeWordWrap];
    [_lineLabel setNumberOfLines:0];
    [_lineLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [self.contentView addSubview:_lineLabel];

    return self;
}

/**
 This function is overridden to support bleed-through of highlighting on selected row.
 */
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // apply highlight colors
    [self applyHighlights];
}

/**
 Set the label text for the cell, and configure any highlighting.
 */
- (void)setLine:(Line *)line forScript:(Script *)script
{
    // set contents of labels
    _actorLabel.text = line.actor;
    _lineLabel.text = [LineCell cellTextForLine:line usingScript:script];

    // identify actor's lines
    if ([[line.actor uppercaseString] isEqualToString:[script.maskActor uppercaseString]]) {
        isActor = YES;
    } else {
        isActor = NO;
    }
    
    // assign highlighting
    self.actorLabel.backgroundColor = [UIColor clearColor];
    if ([line.isHighlighted boolValue]) {
        isHighlight = YES;
    } else {
        isHighlight = NO;
    }
    
    // apply highlight colors
    [self applyHighlights];
}

/**
 Actually apply the color highlighting to this cell.
 */
- (void)applyHighlights
{
    // identify actor's lines in tan
    if (isActor) {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.6 green:0.4 blue:0.2 alpha:0.2];
    } else {
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    
    // assign highlighting in yellow
    if (isHighlight) {
        self.lineLabel.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:0.0 alpha:0.6];
    } else {
        self.lineLabel.backgroundColor = [UIColor clearColor];
    }
    
}

/**
 This section reads the actor's line from the database, or the appropriate cipher.  If cipher is requested, but does not exist in the database, it is calculated automatically.  Once the text is loaded, it is masked appropriately as well.
 */
+ (NSString *)cellTextForLine:(Line *)line usingScript:(Script *)script
{
    // function variable
    NSString *lineText;
    
    // get the line text, cipher text, or automatic cipher as needed
    if ([script.displayCipher boolValue]) {
        if (line.cipher == nil) {
            lineText = [line.text generateCipher];
        } else {
            lineText = line.cipher;
        }
    } else {
        lineText = line.text;
    }

    // set the return value, in case masking is not applied
    NSString* cellText = lineText;
    
    // create variables and defaults for masking process
    NSUInteger lineLength = [lineText length];
    NSUInteger patternIndex = 0;
    NSUInteger patternLength = [script.maskPatternLength unsignedIntValue];
    NSUInteger maskPattern = [script.maskPattern unsignedIntValue];
    NSUInteger maskReplacementLength = [script.maskReplacementLength unsignedIntValue];
    BOOL inWord = NO;
    BOOL isWordChar;
    NSUInteger wordLength = 0;
    NSUInteger index;
    unichar charAtIndex;
    NSMutableString *maskedLine = [NSMutableString stringWithString:@""];
    
    // apply masking if masking on
    if ([script.displayMask boolValue]) {
        // only apply if this is the correct actor
        if ([[line.actor uppercaseString] isEqualToString:[script.maskActor uppercaseString]]) {
            // for each character in the original text
            for (index = 0; index < lineLength; index++) {
                // get a character
                charAtIndex = [lineText characterAtIndex:index];
                // check if it's a letter, hyphen or apostrophe
                isWordChar = (((charAtIndex >= 'a') && (charAtIndex <= 'z'))
                              || ((charAtIndex >= 'A') && (charAtIndex <= 'Z'))
                              || (charAtIndex == '-') || (charAtIndex == '\''));
                if (inWord) {
                    if (!isWordChar) {
                        // if we were in a word, but this isn't a word char, we're not in a word now; move to the next pattern index
                        inWord = NO;
                        patternIndex = (++patternIndex) % patternLength;
                    }
                } else {
                    if (isWordChar) {
                        // change from not word, to a word; reset length
                        inWord = YES;
                        wordLength = 0;
                    }
                }
                if (inWord) {
                    if ((maskPattern & 1<<patternIndex) > 0) {
                        // in a word, and this is a masked word
                        if (wordLength++ < maskReplacementLength || maskReplacementLength == 0) {
                            // not cropping by replacement length
                            if ([script.maskCharacter length] == 0) {
                                // no mask character (cipher mode)
                                [maskedLine appendString:[NSString stringWithFormat:@"%c",charAtIndex]];
                            } else {
                                // mask the character (mask mode)
                                [maskedLine appendString:script.maskCharacter];
                            }
                        } // else: character dropped due to cipher mode
                    } else {
                        // not a masked word; let the letter through
                        [maskedLine appendString:[NSString stringWithFormat:@"%c",charAtIndex]];
                    }
                } else {
                    // not in a word; masks do not apply
                    [maskedLine appendString:[NSString stringWithFormat:@"%c",charAtIndex]];
                }
            }
            // save the result of all this masking
            cellText = maskedLine;
        }
    }

    // return the fully rendered text
    return cellText;
}


@end
