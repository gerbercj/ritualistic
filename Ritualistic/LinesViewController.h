//
//  LinesViewController.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LineAddView.h"
#import "MaskConfigView.h"

@interface LinesViewController : UITableViewController <LineAddDelegate, MaskConfigDelegate, UIScrollViewDelegate, NSFetchedResultsControllerDelegate>
{
@private
    BOOL changeIsUserDriven;
    NSIndexPath *centerRow;
}

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObject *referringObject;
@property (nonatomic, strong) UISegmentedControl *highlightControl;
@property (nonatomic, strong) UISegmentedControl *actorControl;
@property (nonatomic, strong) UIBarButtonItem *hintButton;

- (void)requestUserAbort:(NSError *)error;
- (void)dialogHint;
- (void)updateScriptMask;
- (void)updateActor;
- (void)updateHighlight;
- (void)updateControlsForSelection;
- (NSIndexPath *)indexPathForPredicate:(NSPredicate *)predicate findMin:(BOOL)findMin;
- (NSIndexPath *)previousHighlightFromIndexPath:(NSIndexPath *)indexPath;
- (NSIndexPath *)nextHighlightFromIndexPath:(NSIndexPath *)indexPath;
- (NSIndexPath *)previousLineForActor:(NSString *)actor fromIndexPath:(NSIndexPath *)indexPath;
- (NSIndexPath *)nextLineForActor:(NSString *)actor fromIndexPath:(NSIndexPath *)indexPath;

@end
