//
//  LinesViewController.m
//  Ritualistic
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "LinesViewController.h"
#import "Line.h"
#import "LineAddView.h"
#import "LineCell.h"
#import "Script.h"
#import "HintView.h"

#define LINECELL_FONT_SIZE 12.0

@interface LinesViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation LinesViewController

@synthesize fetchedResultsController=__fetchedResultsController;
@synthesize referringObject=_referringObject;
@synthesize highlightControl=_highlightControl;
@synthesize actorControl=_actorControl;
@synthesize hintButton=_hintButton;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // initialize variable for row reordering
    changeIsUserDriven = NO;
    
    // paper view
    self.view.backgroundColor = [UIColor clearColor];
    self.tableView.separatorColor = [UIColor brownColor];
    
    // setup the edit button
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // create the flexible space button
    UIBarButtonItem *flexibleSpaceButtonItem = [[UIBarButtonItem alloc]
                                                initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                target:nil action:nil];
    
    // create the dialog help button
    UIBarButtonItem *hintButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"DialogHint.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(dialogHint)];
    self.hintButton = hintButton;
    
    // configure the highlight control
    UISegmentedControl *highlightControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:[UIImage imageNamed:@"ArrowUp.png"], [UIImage imageNamed:@"Highlight.png"], [UIImage imageNamed:@"ArrowDown.png"], nil]];
    [highlightControl setMomentary:YES];
    [highlightControl setSegmentedControlStyle:UISegmentedControlStyleBar];
    [highlightControl setTintColor:[UIColor colorWithRed:(115.5/256.0) green:(141.5/256.0) blue:(178.0/256.0) alpha:1]];
    [highlightControl addTarget:self action:@selector(updateHighlight) forControlEvents:UIControlEventValueChanged];
    UIBarButtonItem *highlightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:highlightControl];
    self.highlightControl = highlightControl;
    
    // configure the actor control
    UISegmentedControl *actorControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:[UIImage imageNamed:@"ArrowUp.png"], [UIImage imageNamed:@"Freemason.png"], [UIImage imageNamed:@"ArrowDown.png"], nil]];
    [actorControl setMomentary:YES];
    [actorControl setSegmentedControlStyle:UISegmentedControlStyleBar];
    [actorControl setTintColor:[UIColor colorWithRed:(115.5/256.0) green:(141.5/256.0) blue:(178.0/256.0) alpha:1]];
    [actorControl addTarget:self action:@selector(updateActor) forControlEvents:UIControlEventValueChanged];
    UIBarButtonItem *actorButtonItem = [[UIBarButtonItem alloc] initWithCustomView:actorControl];
    self.actorControl = actorControl;
    
    // create the mask settings button
    UIBarButtonItem *maskButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Settings.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(updateScriptMask)];
        
    // create the toolbar
    NSArray *toolbarItems = [NSArray arrayWithObjects:
                             hintButton,
                             flexibleSpaceButtonItem,
                             highlightButtonItem,
                             flexibleSpaceButtonItem,
                             actorButtonItem,
                             flexibleSpaceButtonItem,
                             maskButtonItem,
                             nil];
    self.toolbarItems = toolbarItems;
    
    // can't use segmented controls until a row is selected
    [self updateControlsForSelection];
    
    // decrement retain counts for buttons in toolbars, as toolbar is retaining
}

- (void)viewWillAppear:(BOOL)animated
{
    // create the toolbar at the bottom
    [self.navigationController setToolbarHidden:NO animated:YES];
    [self updateControlsForSelection];
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    // hide the toolbar at the bottom
    [self.navigationController setToolbarHidden:YES animated:YES];
    
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.highlightControl = nil;
    self.actorControl = nil;
    self.hintButton = nil;
    self.fetchedResultsController = nil;
    self.referringObject = nil;
}

/**
 Responds to requests to toggle between standard and editing mode, updating the navigation controller buttons and adding per line editing controls.
 */
- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
	
	if (editing) {
		// Create an add "+" button
		UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject)];
		// Add it to the left side of the nav bar
		[self.navigationItem setLeftBarButtonItem:addButton animated:YES];
        
        // Disable toolbar due to selection loss
        [self updateControlsForSelection];
	}
	else {
		// The "Done" button was tapped, so we remove the add button from the left side
		[self.navigationItem setLeftBarButtonItem:nil animated:YES];
	}
}

/**
 In addition to its normal use, this function caches the indexpath of the line that is currently in the center of the screen.
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // save a pointer to the center row (to support variable row height)
    CGPoint center = [self.tableView center];
    center.y += self.tableView.bounds.origin.y;
    centerRow = [self.tableView indexPathForRowAtPoint:center];
    
    // return supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

/**
 This function moves the old center row back to the center of the display, and updates the toolbar.
 */
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    // restore center row to center (to support variable row heights)
    [self.tableView scrollToRowAtIndexPath:centerRow atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    
    // update toolbar after every rotation event
    [self updateControlsForSelection];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}


#pragma mark - Scroll View Delegate

/**
 This function is overridden to allow updating the toolbar after every scroll event.
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // update toolbar after every scroll event
    [self updateControlsForSelection];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"lineCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        // create a custom cell with our predetermined font size
        cell = [[LineCell alloc] initWithCellIdentifier:CellIdentifier andFontSize:LINECELL_FONT_SIZE];
    }
    
    // Configure the cell.
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // fetch row from table and create cell using masking settings
    Line *line = [self.fetchedResultsController objectAtIndexPath:indexPath];
    LineCell *newCell = (LineCell *)cell;
    [newCell setLine:line forScript:(Script *)self.referringObject];
    
    // create the disclosure button when editing
	cell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
}

/**
 This function calculates the custom row heights for each row, based upon the actor's line as it will be displayed.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get the text to use to calculate cell/label dimensions
    Line *line = [self.fetchedResultsController objectAtIndexPath:indexPath];
    Script *script = (Script *)self.referringObject;
    NSString *text = [LineCell cellTextForLine:line usingScript:script];
    
    // get the real width
    CGFloat width = tableView.frame.size.width - 85;
    
    // calculate necessary label height
	CGFloat height = [text sizeWithFont:[UIFont systemFontOfSize:LINECELL_FONT_SIZE] constrainedToSize:CGSizeMake(width,2010) lineBreakMode:UILineBreakModeWordWrap].height;
    
    // add top and bottom margins to row height	
    height += 20;
    
    // as per the Apple documentation, don't return a value greather than 2009
    if (height > 2009) {
        return 2009;
    }
    
	return height; 
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        // Delete the managed object for the given index path
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        Line *line = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSUInteger lineIndex = [line.index longValue];
        [context deleteObject:line];
        
        // update higher index numbers
        NSUInteger linesCount = [self.fetchedResultsController.fetchedObjects count];
        for (NSUInteger i=lineIndex; i<linesCount; i++) {
            Line *lineToUpdate = [self.fetchedResultsController.fetchedObjects objectAtIndex:i];
            lineToUpdate.index = [NSNumber numberWithLong:[lineToUpdate.index longValue]-1];
        }
        
        // Save the context.
        NSError *error = nil;
        if (![context save:&error])
        {
            [self requestUserAbort:error];
        }
    }   
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

/**
 This function updates the Core Data model to store the new order of the rows.
 */
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    // based upon http://stackoverflow.com/questions/1077568/how-to-implement-re-ordering-of-coredata-records
    // and also http://stackoverflow.com/questions/1648223/how-can-i-maintain-display-order-in-uitableview-using-core-data
    
    changeIsUserDriven = YES;
    
    NSUInteger sourceIndex = sourceIndexPath.row;  
    NSUInteger destinationIndex = destinationIndexPath.row;
    
    // as long as something was actually moved
    if (sourceIndex != destinationIndex) {
        Line *movedLine = [self.fetchedResultsController.fetchedObjects objectAtIndex:sourceIndex];  
        movedLine.index = [NSNumber numberWithLong:destinationIndex];
        
        NSUInteger start, end;
        NSInteger delta;
        
        if (sourceIndex < destinationIndex) {
            // move was down, need to shift up
            delta = -1;
            start = sourceIndex + 1;
            end = destinationIndex;
        } else { // fromIndex > toIndex
            // move was up, need to shift down
            delta = 1;
            start = destinationIndex;
            end = sourceIndex - 1;
        }
        
        for (NSUInteger i = start; i <= end; i++) {
            Line *lineToUpdate = [self.fetchedResultsController.fetchedObjects objectAtIndex:i];  
            lineToUpdate.index = [NSNumber numberWithLong:[lineToUpdate.index longValue]+delta];
        }
    }
    changeIsUserDriven = NO;
}

/**
 This function is overridden to support tap again for deselect.
 */
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // if currently selected row is selected again, deselect instead
    NSIndexPath *currentlySelectedRow = [self.tableView indexPathForSelectedRow];
    if ([indexPath isEqual:currentlySelectedRow]) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        [self updateControlsForSelection];
        return nil;
    }
    
    // otherwise, allow selection
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // update toolbar
    [self updateControlsForSelection];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // update toolbar
    [self updateControlsForSelection];
}

/**
 This function enables the ability to update a lines properties when in editing mode.
 */
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    // get the record editor view
    LineAddView *lineAddView = [[LineAddView alloc] initWithNibName:@"LineAddView" bundle:nil];
    lineAddView.delegate = self;
    
    // populate the existing record data
    Line *line = [self.fetchedResultsController objectAtIndexPath:indexPath];
    lineAddView.isExistingLine = YES;
    lineAddView.line = line;
    
    // display the editor
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:lineAddView];
    [self presentModalViewController:navigationController animated:YES];
    
}

#pragma mark - Line object functions

/**
 Creates a new Core Data object and then provides the user a view to edit its settings.
 */
- (void)insertNewObject
{
    // get the record editor view
    LineAddView *lineAddView = [[LineAddView alloc] initWithNibName:@"LineAddView" bundle:nil];
    lineAddView.delegate = self;

    // create a new line object at end of script and connect to script
    Line *line = [NSEntityDescription insertNewObjectForEntityForName:@"Line" inManagedObjectContext:[self.fetchedResultsController managedObjectContext]];
    line.index = [NSNumber numberWithUnsignedInteger:[self.fetchedResultsController.fetchedObjects count]];
    line.scripts = (Script *)self.referringObject;
    
    // populate the editor with these default values
    lineAddView.line = line;
    
    // display the editor
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:lineAddView];
    [self presentModalViewController:navigationController animated:YES];
    
}

/**
 The delegate that stores the settings for the new object, as appropriate.
 */
- (void)lineAddView:(LineAddView *)lineAddView didAddLine:(Line *)line
{
    // dismiss the editor view
    [self dismissModalViewControllerAnimated:YES];
    
    // if this record was saved, scroll to make it visible
    if (line != nil) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[line.index unsignedIntegerValue] inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

/**
 This function displays a new view with the unmasked, non-cipher version of the line.
 */
- (void)dialogHint
{
    // get the line that is currently selected
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    Line *line = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // deselect the line
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    // create the hint view & initialize with the line
    HintView *hintView = [[HintView alloc] initWithLine:line];
    
    // display the hint
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:hintView];
    [self presentModalViewController:navigationController animated:YES];
    
}

/**
 Display the mask settings view.
 */
- (void)updateScriptMask
{
    // get the script mask configuration view
    MaskConfigView *maskConfigView = [[MaskConfigView alloc] initWithNibName:@"MaskConfigView" bundle:nil];
    maskConfigView.delegate = self;
    
    // configure the view with the saved settings for this script
    Script *script = (Script *)self.referringObject;
    maskConfigView.script = script;
    
    // display the editor
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:maskConfigView];
    [self presentModalViewController:navigationController animated:YES];
    
}

/**
 Update the settings as necessary, as per the user's changes.
 */
- (void)maskConfigView:(MaskConfigView *)maskConfigView didUpdateScript:(Script *)script
{
    // dismiss the script mask configuration view
    [self dismissModalViewControllerAnimated:YES];
    
    // if the settings were saved, redraw the view with these settings
    if (script != nil) {
        [self.tableView reloadData];
    }
    
    // update the toolbar
    [self updateControlsForSelection];
}

/**
 Make the toolbar buttons enabled or disabled, as approriate
 */
- (void)updateControlsForSelection
{
    // function variables
    Line *line;
    NSString *actor;

    // determine the selected row, if any
    BOOL isSelected = YES;
    NSIndexPath *selectedRow = [self.tableView indexPathForSelectedRow];
    
    // determine the location of the pixel currently displayed in the center of the table view
    CGPoint center = self.tableView.center;
    center.y += self.tableView.bounds.origin.y;
    
    if (selectedRow == nil) {
        // if there is no row selected, use the row under the center pixel and the actor from the mask settings
        isSelected = NO;       
        selectedRow = [self.tableView indexPathForRowAtPoint:center];
        actor = [(Script *)self.referringObject maskActor];
    } else {
        // a row was selected, so use that line and the line's actor
        line = [self.fetchedResultsController objectAtIndexPath:selectedRow];
        actor = line.actor;
    }
    
    // hint, highlight, and actor buttons only active if a row is selected
    [self.hintButton setEnabled:isSelected];
    [self.highlightControl setEnabled:isSelected forSegmentAtIndex:1];
    [self.actorControl setEnabled:isSelected forSegmentAtIndex:1];

    // previous/next highlight availability based upon Core Data search
    [self.highlightControl setEnabled:([self previousHighlightFromIndexPath:selectedRow]!=nil) forSegmentAtIndex:0];
    [self.highlightControl setEnabled:([self nextHighlightFromIndexPath:selectedRow]!=nil) forSegmentAtIndex:2];
    
    // previous/next actor's line availability based upon Core Data search
    [self.actorControl setEnabled:([self previousLineForActor:actor fromIndexPath:selectedRow]!=nil) forSegmentAtIndex:0];
    [self.actorControl setEnabled:([self nextLineForActor:actor fromIndexPath:selectedRow]!=nil) forSegmentAtIndex:2];
}

/**
 Change the actor used for masking based upon the currently selected row
 */
- (void)updateActor
{
    // function variables
    Line *line;
    NSString *actor;
    NSError *error = nil;
    NSIndexPath *scrollIndexPath;
    
    // determine the selected row, if any
    BOOL isSelected=YES;
    NSIndexPath *selectedRow = [self.tableView indexPathForSelectedRow];
    
    // determine the location of the pixel currently displayed in the center of the table view
    CGPoint center = [self.tableView center];
    center.y += self.tableView.bounds.origin.y;
    
    if (selectedRow == nil) {
        // if there is no row selected, use the row under the center pixel and the actor from the mask settings
        isSelected = NO;
        selectedRow = [self.tableView indexPathForRowAtPoint:center];
        actor = [(Script *)self.referringObject maskActor];
    } else {
        // a row was selected, so use that line and the line's actor
        line = [self.fetchedResultsController objectAtIndexPath:selectedRow];
        actor = line.actor;
    }
    
    // use the script from the parent view
    Script *script = (Script *)self.referringObject;
    
    switch ([self.actorControl selectedSegmentIndex]) {
        case 0:
            // find actor's previous line
            scrollIndexPath = [self previousLineForActor:actor fromIndexPath:selectedRow];
            
            // scroll to it
            [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            
            if (isSelected) {
                // only select new line if original line was selected
                [self.tableView selectRowAtIndexPath:scrollIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            break;
            
        case 1:
            // use selected actor for masking, or deselect if same
            if ([script.maskActor isEqualToString:actor]) {
                script.maskActor = nil;
            } else {
                script.maskActor = actor;
            }
            
            // save change to Core Data
            if (![script.managedObjectContext save:&error]) {
                [self requestUserAbort:error];
            }	
            
            // clear selection
            [self.tableView deselectRowAtIndexPath:selectedRow animated:NO];
            
            // update toolbar
            [self updateControlsForSelection];
            
            // refresh all cells to highlight actor
            [self.tableView reloadData];
            
            break;
            
        case 2:
            // find actor's next line
            scrollIndexPath = [self nextLineForActor:actor fromIndexPath:selectedRow];
            
            // scroll to it
            [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            
            if (isSelected) {
                // only select new line if original line was selected
                [self.tableView selectRowAtIndexPath:scrollIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            break;
            
        default:
            break;
    }
}

/**
 Toggle the highlighting for the currently selected row
 */
- (void)updateHighlight
{
    // function variables
    NSError *error = nil;
    NSIndexPath *scrollIndexPath;

    // determine the selected row, if any
    BOOL isSelected = YES;
    NSIndexPath *selectedRow = [self.tableView indexPathForSelectedRow];
    
    // determine the location of the pixel currently displayed in the center of the table view
    CGPoint center = [self.tableView center];
    center.y += self.tableView.bounds.origin.y;
    
    if (selectedRow == nil) {
        // if there is no row selected, use the row under the center pixel
        isSelected = NO;
        selectedRow = [self.tableView indexPathForRowAtPoint:center];
    }
    
    Line *line = [self.fetchedResultsController objectAtIndexPath:selectedRow];
    Script *script = (Script *)self.referringObject;
    
    switch ([self.highlightControl selectedSegmentIndex]) {
        case 0:
            // find previous highlight
            scrollIndexPath = [self previousHighlightFromIndexPath:selectedRow];
            
            // scroll to it
            [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            
            if (isSelected) {
                // only select new line if original line was selected
                [self.tableView selectRowAtIndexPath:scrollIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            break;
            
        case 1:
            // highlight selected line
            line.isHighlighted = [NSNumber numberWithBool:![line.isHighlighted boolValue]];
            
            // save change to Core Data
            if (![script.managedObjectContext save:&error]) {
                [self requestUserAbort:error];
            }
            
            // clear selection
            [self.tableView deselectRowAtIndexPath:selectedRow animated:NO];
            
            // update toolbar
            [self updateControlsForSelection];
            
            // repaint all cells
            [self.tableView reloadData];
            
            break;
            
        case 2: // find next highlight
            scrollIndexPath = [self nextHighlightFromIndexPath:selectedRow];
            
            // scroll to it
            [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            
            if (isSelected) {
                // only select new line if original line was selected
                [self.tableView selectRowAtIndexPath:scrollIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            break;
            
        default:
            break;
    }
    
}

/**
 A helper function to standard searching for previous/subsequent "stuff".
 */
- (NSIndexPath *)indexPathForPredicate:(NSPredicate *)predicate findMin:(BOOL)findMin
{
    // create a new Core Data query from the predicate and sort order
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Line"
                                              inManagedObjectContext:[self.referringObject managedObjectContext]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"index"
                                                                   ascending:findMin];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // execute the query
    NSError *error = nil;
    NSArray *fetchedObjects = [[self.referringObject managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    // if nil, the fetch failed
    if (fetchedObjects == nil) {
        [self requestUserAbort:error];
        return nil;
    }
    
    // if 0 records, return a nil indexPath
    if ([fetchedObjects count] < 1) {
        return nil;
    }
    
    // return the minimum (or max) record from the set
    Line *line = (Line *)[fetchedObjects objectAtIndex:0];
    return [NSIndexPath indexPathForRow:[line.index unsignedIntegerValue] inSection:0];
}

- (NSIndexPath *)previousHighlightFromIndexPath:(NSIndexPath *)indexPath
{
    // find the previous highlight from an indexPath
    Script *script = (Script *)self.referringObject;
    NSPredicate *findPredicate = [NSPredicate predicateWithFormat:@"scripts==%@ AND isHighlighted==YES AND index<%u",script,[indexPath row]];
    return [self indexPathForPredicate:findPredicate findMin:NO];
}

- (NSIndexPath *)nextHighlightFromIndexPath:(NSIndexPath *)indexPath
{
    // find the next highlight from an indexPath
    Script *script = (Script *)self.referringObject;
    NSPredicate *findPredicate = [NSPredicate predicateWithFormat:@"scripts==%@ AND isHighlighted==YES AND index>%u",script,[indexPath row]];
    return [self indexPathForPredicate:findPredicate findMin:YES];
}

- (NSIndexPath *)previousLineForActor:(NSString *)actor fromIndexPath:(NSIndexPath *)indexPath
{
    // find the previous actor's line from an indexPath
    Script *script = (Script *)self.referringObject;
    NSPredicate *findPredicate = [NSPredicate predicateWithFormat:@"scripts==%@ AND actor==%@ AND index<%u",script,actor,[indexPath row]];
    return [self indexPathForPredicate:findPredicate findMin:NO];
}

- (NSIndexPath *)nextLineForActor:(NSString *)actor fromIndexPath:(NSIndexPath *)indexPath
{
    // find the next actor's line from an indexPath
    Script *script = (Script *)self.referringObject;
    NSPredicate *findPredicate = [NSPredicate predicateWithFormat:@"scripts==%@ AND actor==%@ AND index>%u",script,actor,[indexPath row]];
    return [self indexPathForPredicate:findPredicate findMin:YES];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (__fetchedResultsController != nil)
    {
        return __fetchedResultsController;
    }
    
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Line" inManagedObjectContext:[self.referringObject managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    // Clear the cache
	[NSFetchedResultsController deleteCacheWithName:@"Lines"];
	
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:50];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Predicate configuration - specify the relationship's property name "program" 
	// checks whether the value of the key "programs" is the same as the value of the object %@ 
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"scripts == %@", self.referringObject];
	[fetchRequest setPredicate:pred];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[self.referringObject managedObjectContext] sectionNameKeyPath:nil cacheName:@"Lines"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error])
    {
        [self requestUserAbort:error];
	}
    
    return __fetchedResultsController;
}    

#pragma mark - Fetched results controller delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    if (changeIsUserDriven) return;
    
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    if (changeIsUserDriven) return;
    
    UITableView *tableView = self.tableView;
    
    switch(type)
    {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (changeIsUserDriven) return;
    [self.tableView endUpdates];
}

#pragma mark - Helper functions

/**
 If an unrecoverable error occurs, notify the user to quit the application
 */
- (void)requestUserAbort:(NSError *)error
{
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unresolved error" message:[NSString stringWithFormat:@"Please press the home button to exit the application. (%@)",[error description]] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alert show];
}

@end
