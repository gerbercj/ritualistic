//
//  MaskConfigView.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/26/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActorPickerView.h"

@protocol MaskConfigDelegate;
@class Script;

@interface MaskConfigView : UIViewController <ActorPickerDelegate>
{
@private
    BOOL keyboardVisible;
    BOOL initialLoad;
}

@property (nonatomic, strong) Script *script;
@property (nonatomic, assign) NSUInteger maskPattern;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UITextField *actorTextField;
@property (nonatomic, strong) IBOutlet UIButton *selectActorButton;
@property (nonatomic, strong) IBOutlet UISwitch *displayCipherSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *displayMaskSwitch;
@property (nonatomic, strong) IBOutlet UISlider *maskPatternLengthSlider;
@property (nonatomic, strong) IBOutlet UILabel *maskPatternLengthLabel;
@property (nonatomic, strong) IBOutlet UISegmentedControl *maskPatternSegmentedControl;
@property (nonatomic, strong) IBOutlet UITextField *replacementCharacterTextField;
@property (nonatomic, strong) IBOutlet UISlider *replacementLengthSlider;
@property (nonatomic, strong) IBOutlet UILabel *replacementLengthLabel;
@property (nonatomic, strong) id<MaskConfigDelegate> delegate;
@property (nonatomic, strong) IBOutlet UILabel *maskPatternLengthDescriptionLabel;
@property (nonatomic, strong) IBOutlet UILabel *maskPatternLabel;
@property (nonatomic, strong) IBOutlet UILabel *replacementCharacterLabel;
@property (nonatomic, strong) IBOutlet UILabel *replacementLengthDescriptionLabel;

- (void)refreshMaskPatternControl;

- (void)updateMaskPatternLength:(NSUInteger)length;
- (void)updateReplacementLength:(NSUInteger)length;
- (void)updateDisplayMaskDisabledFields;

- (IBAction)displayMaskChanged:(id)sender;
- (IBAction)maskPatternLengthChanged:(id)sender;
- (IBAction)replacementLengthChanged:(id)sender;
- (IBAction)maskPatternChanged:(id)sender;
- (IBAction)selectActorClicked:(id)sender;

- (void)save;
- (void)cancel;
- (void)requestUserAbort:(NSError *)error;

@end


@protocol MaskConfigDelegate <NSObject>

- (void)maskConfigView:(MaskConfigView *)maskConfigView didUpdateScript:(Script *)script;

@end