//
//  MaskConfigView.m
//  Ritualistic
//
//  Created by Chris Gerber on 4/26/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "MaskConfigView.h"
#import "Script.h"
#import "UIView+FindFirstResponder.h"
#import "ActorPickerView.h"

#define MASK_CONFIG_SCROLL_VIEW_HEIGHT 411
#define MAX_PATTERN_LENGTH 5
#define REPLACEMENT_LENGTH_FOR_ALL 6
#define MASK_TITLE_YES @"✓"
#define MASK_TITLE_NO @""

@implementation MaskConfigView

@synthesize script=_script;
@synthesize maskPattern=_maskPattern;
@synthesize scrollView=_scrollView;
@synthesize actorTextField=_actorTextField;
@synthesize selectActorButton=_selectActorButton;
@synthesize displayCipherSwitch=_displayCipherSwitch;
@synthesize displayMaskSwitch=_displayMaskSwitch;
@synthesize maskPatternLengthSlider=_maskPatternLengthSlider;
@synthesize maskPatternLengthLabel=_maskPatternLengthLabel;
@synthesize maskPatternLabel=_maskPatternLabel;
@synthesize replacementCharacterLabel=_replacementCharacterLabel;
@synthesize replacementLengthDescriptionLabel=_replacementLengthDescriptionLabel;
@synthesize maskPatternSegmentedControl=_maskPatternSegmentedControl;
@synthesize replacementCharacterTextField=_replacementCharacterTextField;
@synthesize replacementLengthSlider=_replacementLengthSlider;
@synthesize replacementLengthLabel=_replacementLengthLabel;
@synthesize delegate=_delegate;
@synthesize maskPatternLengthDescriptionLabel=_maskPatternLengthDescriptionLabel;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set the view title
    self.navigationItem.title = @"Mask Settings";
    
    // create the cancel button
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = cancelButtonItem;
    
    // create the save button
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(save)];
    self.navigationItem.rightBarButtonItem = saveButtonItem;
    
    // only set default values on first load
    initialLoad = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    // keyboard is not visible
    keyboardVisible = NO;

    // register notifiers for keyboard appear and disappear
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (initialLoad) {
        // on first load, populate the fields with the saved values
        _actorTextField.text = _script.maskActor;
        _replacementCharacterTextField.text = _script.maskCharacter;
        [self updateMaskPatternLength:[_script.maskPatternLength unsignedIntValue]];
        [self updateReplacementLength:[_script.maskReplacementLength unsignedIntValue]];
        self.maskPattern = [_script.maskPattern unsignedIntValue];
        self.displayCipherSwitch.on = [_script.displayCipher boolValue];
        self.displayMaskSwitch.on = [_script.displayMask boolValue];
        [self updateDisplayMaskDisabledFields];
        initialLoad = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    // release notifiers for keyboard appear and disappear
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
}

- (void)viewDidUnload
{
    self.script = nil;
    self.actorTextField = nil;
    self.maskPatternLengthSlider = nil;
    self.maskPatternLengthLabel = nil;
    self.maskPatternSegmentedControl = nil;
    self.replacementCharacterTextField = nil;
    self.replacementLengthSlider = nil;
    self.replacementLengthLabel = nil;
    self.delegate = nil;
    self.scrollView = nil;
    self.displayCipherSwitch = nil;
    self.displayMaskSwitch = nil;
    self.maskPatternLengthDescriptionLabel = nil;
    self.maskPatternLabel = nil;
    self.replacementCharacterLabel = nil;
    self.replacementLengthDescriptionLabel = nil;
    self.selectActorButton = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

/**
 Readjust the scrollview height to make sure that user can access all controls.
 */
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    // get the current scrollView bounds
    CGRect bounds = self.scrollView.bounds;
    
    bounds.size.height = MASK_CONFIG_SCROLL_VIEW_HEIGHT;
    
    // save the new bounds
    self.scrollView.contentSize = bounds.size;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}


#pragma mark - Keyboard helper functions

/**
 Only allow a single character for the replacement character
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // only allow a single character for the replacement value
    if ([textField isEqual:self.replacementCharacterTextField]) {
        if ([string length] > 0) {
            textField.text = [[string substringToIndex:1] uppercaseString];
        }
        return NO;
    }
    return YES;
}

/**
 Use the return button to resign the first responder in TextFields.
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // hide the keyboard
    [textField resignFirstResponder];
    return YES;
}

/**
 Resize the scrollview to take into account keyboard size.
 */
- (void)keyboardDidShow:(NSNotification *)notification
{
    // based upon http://iphonedevelopertips.com/user-interface/adjust-textfield-hidden-by-keyboard.html
    
    if (keyboardVisible) return;
    
    // Get the size of the keyboard.
    NSValue* aValue = [[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    NSUInteger keyboardHeight;
    switch (self.interfaceOrientation) {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            keyboardHeight = keyboardSize.height;
            break;
        default:
            keyboardHeight = keyboardSize.width;
            break;
    }
    
    // Resize the scroll view to make room for the keyboard
    CGRect viewFrame = self.scrollView.frame;
    viewFrame.size.height -= keyboardHeight;
    self.scrollView.frame = viewFrame;
    
    // Move the correct control into view
    UIView *firstResponder = self.scrollView.findFirstResponder;
    [self.scrollView scrollRectToVisible:firstResponder.frame animated:YES];
    
    // Keyboard is now visible
    keyboardVisible = YES;
}

/**
 Resize the scrollview to take into account keyboard size.
 */
- (void)keyboardWillHide:(NSNotification *)notification
{
    // based upon http://iphonedevelopertips.com/user-interface/adjust-textfield-hidden-by-keyboard.html
    
    if (!keyboardVisible) return;
    
    // Get the size of the keyboard.
    NSValue* aValue = [[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    NSUInteger keyboardHeight;
    switch (self.interfaceOrientation) {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            keyboardHeight = keyboardSize.height;
            break;
        default:
            keyboardHeight = keyboardSize.width;
            break;
    }
    
    // Reset the height of the scroll view to its original value
    CGRect viewFrame = self.scrollView.frame;
    viewFrame.size.height += keyboardHeight;
    self.scrollView.frame = viewFrame;
    
    // Keyboard is no longer visible
    keyboardVisible = NO;	
}

#pragma mark - UI actions and helpers

/**
 Store the new mask pattern and then trigger a UI update.
 */
- (void)setMaskPattern:(NSUInteger)maskPattern
{
    // set mask pattern and refresh UI
    _maskPattern = maskPattern;
    [self refreshMaskPatternControl];
}

/**
 Redraw the UI for the mask pattern.
 */
- (void)refreshMaskPatternControl
{
    // update the segments based upon the bits in the mask pattern
    for (int index=0; index<MAX_PATTERN_LENGTH && index<_maskPatternSegmentedControl.numberOfSegments; index++) {
        if ((_maskPattern & 1<<index)>0) {
            [_maskPatternSegmentedControl setTitle:MASK_TITLE_YES forSegmentAtIndex:index];
        } else {
            [_maskPatternSegmentedControl setTitle:MASK_TITLE_NO forSegmentAtIndex:index];
        }
    }
}

/**
 Update the slider and label with the new length.
 */
- (void)updateMaskPatternLength:(NSUInteger)length
{
    // update the slider
    self.maskPatternLengthSlider.value = length;
    // update the label
    self.maskPatternLengthLabel.text = [NSString stringWithFormat:@"%i",length];
    // remove any extra segments
    while (self.maskPatternSegmentedControl.numberOfSegments > length) {
        [self.maskPatternSegmentedControl removeSegmentAtIndex:(self.maskPatternSegmentedControl.numberOfSegments-1) animated:YES];
    }
    // add more segments if needed
    while (self.maskPatternSegmentedControl.numberOfSegments < length) {
        [self.maskPatternSegmentedControl insertSegmentWithTitle:@"" atIndex:self.maskPatternSegmentedControl.numberOfSegments animated:YES];
    }
    // update display of segments
    [self refreshMaskPatternControl];
}

/**
 Update the slider and label with the new length.
 */
- (void)updateReplacementLength:(NSUInteger)length
{
    if (length == 0) {
        // use a value of zero to not crop word length
        length = REPLACEMENT_LENGTH_FOR_ALL;
    }
    // update the slider
    self.replacementLengthSlider.value = length;
    if (length == REPLACEMENT_LENGTH_FOR_ALL) {
        // if slider is all the way to the right, display All
        self.replacementLengthLabel.text = @"All";
    } else {
        // otherwise display number of characters to crop at
        self.replacementLengthLabel.text = [NSString stringWithFormat:@"%i",length];
    }
}

/**
 If masking is disabled, disable ability to configure its settings.
 */
- (void)updateDisplayMaskDisabledFields
{
    if (self.displayMaskSwitch.on) {
        // enable mask controls when masking on
        self.maskPatternLengthDescriptionLabel.enabled = YES;
        self.maskPatternLengthSlider.enabled = YES;
        self.maskPatternLengthLabel.enabled = YES;
        self.maskPatternLabel.enabled = YES;
        self.maskPatternSegmentedControl.enabled = YES;
        self.replacementCharacterLabel.enabled = YES;
        self.replacementCharacterTextField.enabled = YES;
        self.replacementLengthDescriptionLabel.enabled = YES;
        self.replacementLengthSlider.enabled = YES;
        self.replacementLengthLabel.enabled = YES;
    } else {
        // disable mask controls when masking off
        self.maskPatternLengthDescriptionLabel.enabled = NO;
        self.maskPatternLengthSlider.enabled = NO;
        self.maskPatternLengthLabel.enabled = NO;
        self.maskPatternLabel.enabled = NO;
        self.maskPatternSegmentedControl.enabled = NO;
        self.replacementCharacterLabel.enabled = NO;
        self.replacementCharacterTextField.enabled = NO;
        self.replacementLengthDescriptionLabel.enabled = NO;
        self.replacementLengthSlider.enabled = NO;
        self.replacementLengthLabel.enabled = NO;
    }    
}

/**
 Enable/disable masking mode for script.
 */
- (IBAction)displayMaskChanged:(id)sender
{
    // after flipping toggle, enable/disable other fields
    [self updateDisplayMaskDisabledFields];
}

/**
 Adjust the number of segments and significant bits for the mask pattern.
 */
- (IBAction)maskPatternLengthChanged:(id)sender
{
    // slider moved; update UI
    [self updateMaskPatternLength:roundf([self.maskPatternLengthSlider value])];
}

/**
 Update the length of the replacement, handling max value as "All".
 */
- (IBAction)replacementLengthChanged:(id)sender
{
    // slider moved; update UI
    [self updateReplacementLength:roundf([self.replacementLengthSlider value])];
}

/**
 Update the mask selections and repaint UI.
 */
- (IBAction)maskPatternChanged:(id)sender
{
    // segment was clicked; update UI
    self.maskPattern ^= 1 << self.maskPatternSegmentedControl.selectedSegmentIndex;
    [self refreshMaskPatternControl];
}

/**
 Select a mask actor using the actor picker.
 */
- (IBAction)selectActorClicked:(id)sender
{
    // get actor picker view
    ActorPickerView *actorPickerView = [[ActorPickerView alloc] initWithNibName:@"ActorPickerView" bundle:nil];
    actorPickerView.delegate = self;
    
    // set the default actor, and the script to load the actors from
    actorPickerView.defaultActor = self.actorTextField.text;
    actorPickerView.script = self.script;
    
    // display the picker
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:actorPickerView];
    [self presentModalViewController:navigationController animated:YES];
    
}

/**
 Delegate to respond to actor picker.
 */
- (void)actorPickerView:(ActorPickerView *)actorPickerView didPickActor:(NSString *)actor
{
    // if the user didn't cancel
    if (actor != nil) {
        // an actor was selected
        if ([actor isEqualToString:@""]) {
            // the "empty" actor ("<none>") was selected, so clear value
            self.actorTextField.text = nil;
        } else {
            // an actual actor was selected, so use it
            self.actorTextField.text = actor;
        }
    }
    
    // hide the view controller
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - Navigation controller actions

/**
 Save the new configuration settings.
 */
- (void)save
{
    // store all the settings in the script object
    self.script.maskActor = self.actorTextField.text;
    self.script.maskCharacter = self.replacementCharacterTextField.text;
    self.script.maskPattern = [NSNumber numberWithUnsignedInteger:self.maskPattern];
    self.script.maskPatternLength = [NSNumber numberWithUnsignedInteger:self.maskPatternLengthSlider.value];
    if (self.replacementLengthSlider.value == REPLACEMENT_LENGTH_FOR_ALL) {
        self.script.maskReplacementLength = [NSNumber numberWithInteger:0];
    } else {
        self.script.maskReplacementLength = [NSNumber numberWithInteger:self.replacementLengthSlider.value];
    }
    self.script.displayCipher = [NSNumber numberWithBool:self.displayCipherSwitch.on];
    self.script.displayMask = [NSNumber numberWithBool:self.displayMaskSwitch.on];
    
    // save the values to the database
	NSError *error = nil;
	if (![self.script.managedObjectContext save:&error]) {
        [self requestUserAbort:error];
	}		
    
    // hide the view
	[self.delegate maskConfigView:self didUpdateScript:self.script];
}

/**
 Abandon the new configuration settings.
 */
- (void)cancel
{
    // don't save; just hide the view
    [self.delegate maskConfigView:self didUpdateScript:nil];
}

#pragma mark - Helper functions

/**
 If an unrecoverable error occurs, notify the user to quit the application
 */
- (void)requestUserAbort:(NSError *)error
{
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unresolved error" message:[NSString stringWithFormat:@"Please press the home button to exit the application. (%@)",[error description]] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alert show];
}

@end
