//
//  NSString+Ritualistic.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/25/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (Ritualistic)
- (NSString *)generateCipher;
- (NSString *)removeExtraWhiteSpaces;
@end
