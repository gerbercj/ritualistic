//
//  NSString+Ritualistic.m
//  Ritualistic
//
//  Created by Chris Gerber on 4/25/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "NSString+Ritualistic.h"


@implementation NSString (Ritualistic)

/**
 This function automatically generates cipher text consisting of the first two word characters, at most, from each word found.
 */
- (NSString *)generateCipher
{
    // function variables
    NSError *error = NULL;
    
    // create a regex to match two word characters, followed by any number of word characters
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(\\w\\w)\\w+" options:NSRegularExpressionCaseInsensitive error:&error];

    // replace the match with just the first two word characters
    return [regex stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, [self length]) withTemplate:@"$1"];
}

/**
 This function removes starting and training whitespace, and collapses all internal sets of multiple whitespace characters down to a single space.
 */
- (NSString *)removeExtraWhiteSpaces
{
    // function variables
    NSUInteger stringLength = [self length];
    char cString[stringLength+1];
    NSUInteger cStringIndex = 0;
    BOOL inStartingWhitespace = YES;
    BOOL inWhitespace = NO;

    // for each character
    for (NSInteger index=0; index< stringLength; index++) {
        // get the character
        char charAtIndex = [self characterAtIndex:index];
        // if a space or a control code
        if (charAtIndex < 33) {
            // but not in before the string started
            if (!inStartingWhitespace) {
                // we need one whitespace character total
                inWhitespace = YES;
            }
        } else {
            // normal character
            if (inWhitespace) {
                // transition from whitespace to non-whitespace character
                if (!inStartingWhitespace) {
                    // insert one space, and the next character
                    cString[cStringIndex] = 32;
                    cString[++cStringIndex] = charAtIndex;
                }
            } else {
                // still in non-whitespace, so insert the character
                cString[cStringIndex] = charAtIndex;
            };
            inStartingWhitespace = NO;
            inWhitespace = NO;
            cStringIndex++;
        }
    }
    
    // force null terminator
    cString[cStringIndex]=0;
    
    // convert back to NSString and return
    return [NSString stringWithUTF8String:cString];
}

@end
