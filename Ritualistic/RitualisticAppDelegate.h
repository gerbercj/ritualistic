//
//  RitualisticAppDelegate.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RitualisticAppDelegate : NSObject <UIApplicationDelegate>
{
}

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) IBOutlet UINavigationController *navigationController;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)requestUserAbort:(NSError *)error;

@end
