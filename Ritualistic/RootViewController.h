//
//  RootViewController.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ScriptAddView.h"
#import "Line.h"

@interface RootViewController : UITableViewController <ScriptAddDelegate, NSFetchedResultsControllerDelegate, NSXMLParserDelegate> {
    NSURLConnection *_connection;
    NSMutableString *capturedCharacters;
    Script *importedScript;
    NSMutableSet *importedLines;
    NSUInteger lineIndex;
    Line *importedLine;
    Line *lastImportedLine;
}

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)downloadXMLFromURL:(NSString *)URL;
- (void)parseDownloadedXML;
- (void)requestUserAbort:(NSError *)error;

@end
