//
//  RootViewController.m
//  Ritualistic
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "RootViewController.h"
#import "Script.h"
#import "ScriptAddView.h"
#import "LinesViewController.h"
#import "Line.h"
#import "NSString+Ritualistic.h"

@implementation RootViewController

@synthesize fetchedResultsController=__fetchedResultsController;
@synthesize managedObjectContext=__managedObjectContext;
@synthesize responseData=_responseData;
@synthesize activityIndicator=_activityIndicator;

#pragma mark - View lifecycle functions

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set up the edit and add buttons
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    // set the title
    self.title = @"Scripts";
    
    // create the add button
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    // create an activity indicator for downloads
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    CGPoint aiCenter = self.tableView.center;
    aiCenter.y = self.tableView.rowHeight / 2.0;
    [self.activityIndicator setCenter:aiCenter];
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.view addSubview:self.activityIndicator];
    
    // make the row separators brown to match paper
    [self.tableView setSeparatorColor:[UIColor brownColor]];
}

- (void)viewDidUnload
{
    self.fetchedResultsController = nil;
    self.managedObjectContext = nil;
    self.responseData = nil;
    self.activityIndicator = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}


#pragma mark - Table View functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    // reuse cell if available, or create new if needed
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // configure the cell
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // place the name of the script in the cell
    Script *script = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = script.name;
    
    // let the paper pattern show through
    cell.opaque = NO;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    
	// add a detail disclosure button for detail view navigation
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        // delete the managed object for the given index path
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        // save the context
        NSError *error = nil;
        if (![context save:&error])
        {
            [self requestUserAbort:error];
        }
    }   
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // the table view should not be re-orderable
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get the current script
    Script *script = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // create the appropriate lines view
    LinesViewController *linesViewController = [[LinesViewController alloc] initWithStyle:UITableViewStylePlain];
    linesViewController.title = script.name;
    linesViewController.referringObject = script;
    
    //display the new view
    [self.navigationController pushViewController:linesViewController animated:YES];
    
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    // get the record editor view
    ScriptAddView *scriptAddView = [[ScriptAddView alloc] initWithNibName:@"ScriptAddView" bundle:nil];
    scriptAddView.delegate = self;
    
    // populate the existing record data
    Script *script = [self.fetchedResultsController objectAtIndexPath:indexPath];
    scriptAddView.isExistingScript = YES;
    scriptAddView.script = script;
    
    // display the editor
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:scriptAddView];
    [self presentModalViewController:navigationController animated:YES];
    
}

#pragma mark - Script object functions

/**
 Creates a new Core Data object and then provides the user a view to edit its settings.
 */
- (void)insertNewObject
{
    // get the record editor view
    ScriptAddView *scriptAddView = [[ScriptAddView alloc] initWithNibName:@"ScriptAddView"bundle:nil];
    scriptAddView.delegate = self;
    
    // create a new empty record
    Script *script = [NSEntityDescription insertNewObjectForEntityForName:@"Script" inManagedObjectContext:self.managedObjectContext];
    scriptAddView.script = script;
    
    // display the editor
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:scriptAddView];
    [self presentModalViewController:navigationController animated:YES];
    
}

/**
 The delegate that stores the settings for the new object, as appropriate.
 */
- (void)scriptAddView:(ScriptAddView *)scriptAddView didAddScript:(Script *)script willDownloadScript:(NSString *)URL
{
    // dismiss the editor view
    [self dismissModalViewControllerAnimated:YES];
    
    // reload the table with the new data
    [self.tableView reloadData];
    
    // if this was a URL, not a script name, kick off the download
    if (URL != nil) {
        importedScript = script;
        [self.activityIndicator startAnimating];
        [self downloadXMLFromURL:URL];
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (__fetchedResultsController != nil)
    {
        return __fetchedResultsController;
    }
    
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Script" inManagedObjectContext:__managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:__managedObjectContext sectionNameKeyPath:nil cacheName:@"Root"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        [self requestUserAbort:error];
	}
    
    return __fetchedResultsController;
}    

#pragma mark - Fetched results controller delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type)
    {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Download URL

// Based upon ideas found in "Professional iPhone and iPad Database Application Programming," by Patrick Alessi

- (void)downloadFailedAlert:(NSString *)message
{
    // generic alert dialog for any failures during download
    UIAlertView *downloadFailedAlert = [[UIAlertView alloc] initWithTitle:@"Download Failed" message:message delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
    [downloadFailedAlert show];
}

/**
 Given a URL this function will download the contents of the URL, and then parse the content with our custom XML parser
 */
- (void)downloadXMLFromURL:(NSString *)URL
{
    // try connecting to the resource
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URL] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:30.0];
    _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (_connection) {
        // if we connected, get store for data
        self.responseData = [NSMutableData data];
    } else {
        // otherwise, tell user about failure
        [self.activityIndicator stopAnimating];
        [self downloadFailedAlert:@"The script could not be downloaded." ];
    }
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
    // use any proposed redirected URL
    return request;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    // ignore authentication
}

- (void)connection:(NSURLConnection *)connection didCancelAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    // ignore authentication
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // discard any response data
    [self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // store any actual received data
    [self.responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    // use any proposed cache response
    return cachedResponse;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // on complete, close connection
    
    // start parsing the data as XML
    [self parseDownloadedXML];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // delete the partial record
    [importedScript.managedObjectContext deleteObject:importedScript];
    
    // save the deletion
    NSError *coreDataError = nil;
    if (![importedScript.managedObjectContext save:&coreDataError]) {
        [self requestUserAbort:coreDataError];
    }
    [self.activityIndicator stopAnimating];

    // tell user about the failure
    [self downloadFailedAlert:[NSString stringWithFormat:@"An error occured while downloading the script:\n%@", [error description]]];
}

#pragma mark - XML parsing

// Based upon ideas found in "Professional iPhone and iPad Database Application Programming," by Patrick Alessi

/**
 This function takes data and attempts to parse it using our custom XML format.
 */
- (void)parseDownloadedXML
{
    // create a new XML parser
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:self.responseData];
    [xmlParser setDelegate:self];
    
    // try parsing the downloaded data
    if (![xmlParser parse]) {
        // delete the partial record
        [importedScript.managedObjectContext deleteObject:importedScript];
        
        // save the deletion
        NSError *error = nil;
        if (![importedScript.managedObjectContext save:&error]) {
            [self requestUserAbort:error];
        }
        [self.activityIndicator stopAnimating];

        // tell the user about the failure
        [self downloadFailedAlert:@"An error occured while parsing the downloaded script."];
    }
    
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    // initialize parsing state machine
    capturedCharacters = nil;
    importedLines = nil;
    lineIndex = 0;
    importedLine = nil;
    lastImportedLine = nil;
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    // if we have an unsaved record, there was an error
    if (importedScript != nil) {
        // delete the partial record
        [importedScript.managedObjectContext deleteObject:importedScript];
        
        // save the deletion
        NSError *error = nil;
        if (![importedScript.managedObjectContext save:&error]) {
            [self requestUserAbort:error];
        }
        [self.activityIndicator stopAnimating];
        
        // tell the user about the failure
        [self downloadFailedAlert:@"The script could not be downloaded."];
    }
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    // script tag open
    if ([elementName isEqualToString:@"script"]) {
        // save the title, or use a generic title
        NSString *thisScriptName = [attributeDict objectForKey:@"title"];
        if (thisScriptName) {
            importedScript.name = thisScriptName;
        } else {
            importedScript.name = @"Imported Script";
        }
        return;
    }
    
    // line tag open
    if ([elementName isEqualToString:@"line"]) {
        // start capturing text between tags
        capturedCharacters = [[NSMutableString alloc] initWithCapacity:1000];
        
        // if we don't have a line record yet, make one
        if (importedLine == nil) {
            importedLine = [NSEntityDescription insertNewObjectForEntityForName:@"Line" inManagedObjectContext:self.managedObjectContext];
            
            // connect it to this script
            importedLine.scripts = importedScript;
            
            // set the line index
            importedLine.index = [NSNumber numberWithUnsignedInteger:lineIndex];
            lineIndex++;
            
            // store this line into the collection of lines
            if (importedLines == nil) {
                importedLines = [[NSMutableSet alloc] initWithObjects:importedLine, nil]; 
            } else {
                [importedLines addObject:importedLine];
            }
        }
        
        // save the provided actor name, or use a dummy
        NSString *thisActor = [attributeDict objectForKey:@"actor"];
        if (thisActor) {
            importedLine.actor = thisActor;
        } else {
            importedLine.actor = @"Unknown";
        }
        return;
    }
    
    // for cipher tag open, just start capturing text
    if ([elementName isEqualToString:@"cipher"]) {
        capturedCharacters = [[NSMutableString alloc] initWithCapacity:1000];
        return;
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    // script tag close
    if ([elementName isEqualToString:@"script"]) {
        // attach captured lines to script
        importedScript.lines = importedLines;
        
        // stop spinner
        [self.activityIndicator stopAnimating];
        
        // save record
        NSError *error = nil;
        if (![importedScript.managedObjectContext save:&error]) {
            [self requestUserAbort:error];
        }
        
        // nil out pointer as a success flag
        importedScript = nil;
        
        // reload the table to display new record
        [self.tableView reloadData];
    }
    
    // line tag close
    if ([elementName isEqualToString:@"line"]) {
        // clean the parsed text
        importedLine.text = [capturedCharacters removeExtraWhiteSpaces];
        
        // generate cipher text, in case none is provided later
        importedLine.cipher = [importedLine.text generateCipher];
        
        // save point to line in case cipher text comes next
        lastImportedLine = importedLine;
        
        // release our handle to line, as it is in the array
        importedLine = nil;
        
        // empty the capture buffer
        capturedCharacters = nil;
    }
    
    // cipher tag close
    if ([elementName isEqualToString:@"cipher"]) {
        // attach the provided cipher text to the last stored line of script
        lastImportedLine.cipher = [capturedCharacters removeExtraWhiteSpaces];
        
        // empty the capture buffer
        capturedCharacters = nil;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    // append found characters to the buffer
    if (capturedCharacters != nil) {
        [capturedCharacters appendString:string];
    }
}

#pragma mark - Helper functions

/**
 If an unrecoverable error occurs, notify the user to quit the application
 */
- (void)requestUserAbort:(NSError *)error
{
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unresolved error" message:[NSString stringWithFormat:@"Please press the home button to exit the application. (%@)",[error description]] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alert show];
}

@end
