//
//  Script.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/26/11.
//  Copyright (c) 2011 theGerb.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Line;

@interface Script : NSManagedObject {
@private
}
@property (nonatomic, strong) NSNumber * displayCipher;
@property (nonatomic, strong) NSString * maskCharacter;
@property (nonatomic, strong) NSString * maskActor;
@property (nonatomic, strong) NSNumber * maskPattern;
@property (nonatomic, strong) NSNumber * maskReplacementLength;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSNumber * maskPatternLength;
@property (nonatomic, strong) NSNumber * displayMask;
@property (nonatomic, strong) NSSet* lines;

@end
