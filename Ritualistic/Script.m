//
//  Script.m
//  Ritualistic
//
//  Created by Chris Gerber on 4/26/11.
//  Copyright (c) 2011 theGerb.com. All rights reserved.
//

#import "Script.h"
#import "Line.h"


@implementation Script
@dynamic displayCipher;
@dynamic maskCharacter;
@dynamic maskActor;
@dynamic maskPattern;
@dynamic maskReplacementLength;
@dynamic name;
@dynamic maskPatternLength;
@dynamic displayMask;
@dynamic lines;

- (void)addLinesObject:(Line *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"lines" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"lines"] addObject:value];
    [self didChangeValueForKey:@"lines" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
}

- (void)removeLinesObject:(Line *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"lines" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"lines"] removeObject:value];
    [self didChangeValueForKey:@"lines" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
}

- (void)addLines:(NSSet *)value {    
    [self willChangeValueForKey:@"lines" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"lines"] unionSet:value];
    [self didChangeValueForKey:@"lines" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeLines:(NSSet *)value {
    [self willChangeValueForKey:@"lines" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"lines"] minusSet:value];
    [self didChangeValueForKey:@"lines" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


@end
