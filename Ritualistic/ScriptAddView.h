//
//  ScriptAddView.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/23/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ScriptAddDelegate;
@class Script;


@interface ScriptAddView : UIViewController <UITextFieldDelegate>
{
}

@property (nonatomic, strong) Script *script;
@property (nonatomic, assign) BOOL isExistingScript;
@property (nonatomic, strong) IBOutlet UISegmentedControl *scriptTypeControl;
@property (nonatomic, strong) IBOutlet UITextField *nameTextField;
@property (nonatomic, strong) id<ScriptAddDelegate> delegate;

- (IBAction)scriptTypeChanged:(id)sender;

- (void)save;
- (void)cancel;
- (void)requestUserAbort:(NSError *)error;

@end


@protocol ScriptAddDelegate <NSObject>

- (void)scriptAddView:(ScriptAddView *) scriptAddView didAddScript:(Script *)script willDownloadScript:(NSString *)URL;

@end
