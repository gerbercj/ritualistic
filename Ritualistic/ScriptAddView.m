//
//  ScriptAddView.m
//  Ritualistic
//
//  Created by Chris Gerber on 4/23/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "ScriptAddView.h"
#import "Script.h"

@implementation ScriptAddView

@synthesize script=_script;
@synthesize scriptTypeControl=_scriptTypeControl;
@synthesize isExistingScript=_isExistingScript;
@synthesize nameTextField=_nameTextField;
@synthesize delegate=_delegate;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // set the title
    self.navigationItem.title = @"Add Script";
    
    // create the cancel button
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = cancelButtonItem;
    
    // create the save button
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(save)];
    self.navigationItem.rightBarButtonItem = saveButtonItem;

    // activate the name field
    [self.nameTextField becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    // default the text field settings
    [self scriptTypeChanged:0];

    // leave the fields blank, unless we're editing an existing line
    if (_isExistingScript) {
        _nameTextField.text = _script.name;
        _scriptTypeControl.hidden = YES;
    }
}

- (void)viewDidUnload
{
    self.script = nil;
    self.nameTextField = nil;
    self.delegate = nil;
    self.scriptTypeControl = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}


#pragma mark - Text Field Delegate

/**
 Automatically hide keyboard and save value on Return
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // hide the keyboard and save (or download) the value
    [textField resignFirstResponder];
    [self save];
	return YES;
}

#pragma mark - UI Actions

/**
 Toggle between create empty script, and download from URL mode.
 */
- (IBAction)scriptTypeChanged:(id)sender
{
    switch (self.scriptTypeControl.selectedSegmentIndex) {
        case 0: // configure field for new blank script
            self.nameTextField.placeholder = @"Script Name";
            self.nameTextField.text = @"";
            self.nameTextField.keyboardType = UIKeyboardTypeASCIICapable;
            self.nameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            self.nameTextField.autocorrectionType = UITextAutocorrectionTypeYes;
            [self.nameTextField reloadInputViews];
            break;
            
        case 1: // configure field for download script from URL
            self.nameTextField.placeholder = @"Script URL";
            self.nameTextField.text = @"";
            self.nameTextField.keyboardType = UIKeyboardTypeURL;
            self.nameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            self.nameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            [self.nameTextField reloadInputViews];
            break;
            
        default:
            break;
    }
}

#pragma mark - Navigation controller actions

/**
 Save the new script entry, save script entry edits, or start the download.
 */
- (void)save
{
    NSError *error = nil;
    switch (self.scriptTypeControl.selectedSegmentIndex) {
        case 0: // save new blank script
            // set the script name
            self.script.name = self.nameTextField.text;
            
            // attempt to save the record
            if (![self.script.managedObjectContext save:&error]) {
                [self requestUserAbort:error];
            }		
            
            // tell the view to close
            [self.delegate scriptAddView:self didAddScript:self.script willDownloadScript:nil];
            break;
            
        case 1: // download script from URL
            [self.delegate scriptAddView:self didAddScript:self.script willDownloadScript:self.nameTextField.text];
            break;
            
        default:
            break;
    }
}

/**
 Abandon the newly created script entry.
 */
- (void)cancel
{
	if (!self.isExistingScript) {
        // only delete script if it was a new blank, not existing
        [self.script.managedObjectContext deleteObject:self.script];
        
        NSError *error = nil;
        if (![self.script.managedObjectContext save:&error]) {
            [self requestUserAbort:error];
        }		
    }
    
    // close without update or download
    [self.delegate scriptAddView:self didAddScript:nil willDownloadScript:nil];
}

#pragma mark - Helper functions

/**
 If an unrecoverable error occurs, notify the user to quit the application
 */
- (void)requestUserAbort:(NSError *)error
{
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unresolved error" message:[NSString stringWithFormat:@"Please press the home button to exit the application. (%@)",[error description]] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alert show];
}

@end
