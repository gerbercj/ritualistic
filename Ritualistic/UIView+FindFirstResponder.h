//
//  UIView+FindFirstResponder.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/25/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIView (UIView_FindFirstResponder)

- (UIView *)findFirstResponder;

@end
