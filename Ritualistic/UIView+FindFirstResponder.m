//
//  UIView+FindFirstResponder.m
//  Ritualistic
//
//  Created by Chris Gerber on 4/25/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "UIView+FindFirstResponder.h"

@implementation UIView (FindFirstResponder)

/**
 This function recusively searches for a first responder, returning its view if it finds it.
 */
- (UIView *)findFirstResponder
{
    // based upon http://stackoverflow.com/questions/1823317/how-do-i-legally-get-the-current-first-responder-on-the-screen-on-an-iphone
    
    // if this is the first responder, we're done
    if (self.isFirstResponder) {        
        return self;     
    }
    
    // for each of the subviews
    for (UIView *subView in self.subviews) {
        // recursively check for first responder
        UIView *firstResponder = [subView findFirstResponder];
        
        // if found, return it
        if (firstResponder != nil) {
            return firstResponder;
        }
    }
    
    // not found; return nil
    return nil;
}

@end
