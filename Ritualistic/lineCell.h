//
//  LineCell.h
//  Ritualistic
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Line.h"
#import "Script.h"

@interface LineCell : UITableViewCell {
}

@property (nonatomic, retain) IBOutlet UILabel *actorLabel;
@property (nonatomic, retain) IBOutlet UILabel *lineLabel;

- (id)initWithCellIdentifier:(NSString *)cellIdentifier andFontSize:(CGFloat)fontSize;
- (void)setLine:(Line *)line forScript:(Script *)script;
+ (NSString *)cellTextForLine:(Line *)line usingScript:(Script *)script;

@end
