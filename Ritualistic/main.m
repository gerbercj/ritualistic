//
//  main.m
//  Ritualistic
//
//  Created by Chris Gerber <chris@theGerb.com> on 4/22/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//
//  HUID: 90790579
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, nil);
        return retVal;
    }
}
