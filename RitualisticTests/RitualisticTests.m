//
//  RitualisticTests.m
//  RitualisticTests
//
//  Created by Chris Gerber on 4/22/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "RitualisticTests.h"
#import "NSString+Ritualistic.h"

@implementation RitualisticTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    NSString *lineText = @"This is a test";
    STAssertEqualObjects([lineText generateCipher], @"Th is a te", @"Automatic cipher", nil);
}

@end
